<?php

class Admin_Controller extends MY_Controller
{
    function __construct()
    { 
        parent::__construct();

        // START DYNAMICALLY ADD STYLESHEETS
        $css = array(
            $this->admin_assets_path . 'default/css/bootstrap.css',
            $this->admin_assets_path . 'default/css/bootstrap-responsive.css',
            $this->admin_assets_path . 'default/css/docs.css',
            $this->admin_assets_path . 'default/css/admin.css'
        );

        $this->template->stylesheet->add($css);
        // END DYNAMICALLY ADD STYLESHEETS

        // START DYNAMICALLY ADD JAVASCRIPTS
        $js = array(
            $this->admin_assets_path . 'default/js/jquery.js',
            $this->admin_assets_path . 'default/js/bootstrap-dropdown.js',
            $this->admin_assets_path . 'default/js/bootstrap-tab.js',
            $this->admin_assets_path . 'default/js/bootstrap.min.js',
            $this->admin_assets_path . 'default/js/ckeditor/ckeditor.js',
            $this->admin_assets_path . 'default/js/jquery-ui.js',
            $this->admin_assets_path . 'default/js/default.js'
        );

        $this->template->javascript->add($js);
        // END DYNAMICALLY ADD STYLESHEETS       

        $this->load->library('email', array('mailtype' => 'html'));
        $this->template->set_template('templates/admin/default/index');
    }

    protected function _do_upload($config = array())
    {
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());

            return $error;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());

            return $data;
        }
    }

    protected function _do_resize_img($source_img_path, $copy_img_path)
    {
        $config['image_library']    = 'gd2';
        $config['source_image']     = $source_img_path;
        $config['new_image']        = $copy_img_path;
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = TRUE;
        $config['height']           = 80;
        $config['width']            = 80;
        $config['thumb_marker']     = '';

        $this->load->library('image_lib', $config); 

        if ( ! $this->image_lib->resize())
        {
            return $this->image_lib->display_errors();
        }        
    }    
}