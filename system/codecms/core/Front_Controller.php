<?php

class Front_Controller extends MY_Controller
{
    function __construct(){

        parent::__construct();   

        $this->template->meta->add('robots', 'index,no-follow');
        $this->template->meta->add('description', 'iBaby Photography specializes in creating the perfect setting and look just right for maternity, newborn, baby and family modern portraiture.');
        $this->template->meta->add('tags', 'photography, baby, babies, newborn, christening, child dedication, baptism, baptismal, birthday, party, outdoor, indoor, canon, dslr');

        $this->template->set_template('templates/public/default/index');


        // START DYNAMICALLY ADD STYLESHEETS
        $css = array(
            'templates/public/default/bootstrap3/css/bootstrap.min.css',
            'templates/public/default/bootstrap3/css/style.css',
            'templates/public/default/bootstrap3/css/resp.css',
            'templates/public/default/bootstrap3/css/flexslider.css'
        );

        $this->template->stylesheet->add($css);
        // END DYNAMICALLY ADD STYLESHEETS               

        // START DYNAMICALLY ADD JAVASCRIPTS
        $js = array(
            'http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js',
            'templates/public/default/bootstrap3/js/bootstrap.min.js',
            'templates/public/default/bootstrap3/js/modernizr.js',
            'templates/public/default/bootstrap3/js/jquery.flexslider.js',
            'templates/public/default/bootstrap3/js/default.js',
        );

        $this->template->javascript->add($js);
        // END DYNAMICALLY ADD STYLESHEETS        
    }
}