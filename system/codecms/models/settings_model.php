<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Settings_model extends CI_Model {	

	public $database 			= 'codecms';
	public $posts_table 		= 'posts';
	public $settings_table 		= 'settings';
	public $users_table 		= 'users';
	public $cats_table 			= 'categories';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();        
    }	

	/* ===============================================================	BACK END =============================================================== */	
	public function get_setting( $where ) 
	{
		$this->db->select('settings_value');
		$this->db->where('settings_name', $where);
		$query = $this->db->get('settings');

		foreach ($query->result_array() as $value) :
			$result = $value['settings_value'];
		endforeach;
		
		return $result;
	}	

	public function update_post_settings()
	{

		$data = array(
		    array(
		        'settings_id' 		=> 1,
		        'settings_name' 	=> 'POST_PAGE_CHOSEN',
		        'settings_value' 	=> $this->input->post('POST_PAGE_CHOSEN')
		    ),
		    array(
		        'settings_id' 		=> 2,
		        'settings_name'		=> 'POST_PER_PAGE',
		        'settings_value' 	=> $this->input->post('POST_PER_PAGE')
		    ),
		    array(
		        'settings_id' 		=> 3,
		        'settings_name' 	=> 'ARRANGE_POST_BY',
		        'settings_value' 	=> $this->input->post('ARRANGE_POST_BY')
		   	),
		   	array(
		        'settings_id' 		=> 4,
		        'settings_name' 	=> 'ORDER_POST_BY',
		        'settings_value' 	=> $this->input->post('ORDER_POST_BY')
		   	),
		   	array(
		        'settings_id' 		=> 5,
		        'settings_name' 	=> 'DEFAULT_EMAIL',
		        'settings_value' 	=> $this->input->post('DEFAULT_EMAIL')
		   	)		    
		);

		$this->db->update_batch('settings', $data, 'settings_id');
	}

	public function settings() 
	{
		$query = $this->db->get('settings');

		if ( $query->num_rows() > 0 ) :
			return $query->result_array();
		endif;	
	}
}