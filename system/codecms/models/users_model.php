<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Users_model extends CI_Model {

    public $database            = 'codecms';
    public $posts_table         = 'posts';
    public $settings_table      = 'settings';
    public $table               = 'users';    

    function __construct() {
        
        // Call the Model constructor
        parent::__construct();
    }

    /* INSERT IDENTITY ON LOGIN */    
    function insert_identity()
    {
        $data = $this->session->all_userdata();

        foreach ($data as $udata) :
            $user_identity = array( 'identity' => sha1($udata), 'is_logged_in' => 1, 'last_login' => time($this->session->userdata('last_activity')) );
        endforeach;

        $this->db->where('email', $this->session->userdata('email'));
        $this->db->update('users', $user_identity);
    }

    function checkRole()
    {
        $this->db->select('role');
        $query = $this->db->get_where('users', array('email' => $this->input->post('email')));
        if ( $query->num_rows() > 0 ) :

            foreach ($query->result() as $result) :
                $role = $result->role;
            endforeach;
            return $role;
        endif;
    }

	/* READING THE USERS LIST */
	function users()
    {
        $query = $this->db->get('users');
        if ( $query->num_rows() > 0 ) :
            return $users_list = $query->result_array();
        endif;
	}

    /* QUERY THE SPECIFIC USER */
    function user_query() {

        $query = $this->db->get_where('users', array('users_id' => $this->uri->segment(4,0)));

        if ( $query->num_rows() > 0 ) :
            $data = $query->result();
            return $data;
        endif;
    }

    /* RETRIEVES THE CURRENT USER INFORMATION */
    function logged_in() {

        $query = $this->db->get_where('users', array('email' => $this->session->userdata('email')));

        if($query->num_rows() > 0):
            foreach ($query->result_array() as $row) :
                $data = $row;
            endforeach;            
            return $data;
        endif;
    }

    /* LOGS OUT A USER */
    function logout() {

        $this->db->set('identity', 0 );
        $this->db->set('is_logged_in', 0 );
        $this->db->where('email', $this->session->userdata('email'));
        $this->db->update('users');

        $this->session->sess_destroy();
    }

    /* FORGOT PASSWORD CHECKING FOR THE EXISTING EMAIL OF THE USER. */
    function user_exist($email)
    {
        $query = $this->db->get_where('users', array('email' => $email));
        
        if($query->num_rows() > 0)
            return (bool)$query->result();
    }

    function generateKey($email, $key)
    {   
        $data = array('pw_recovery' => $key);

        $this->db->where('email', $email);
        $this->db->update('users', $data);        
    }

    /* CHECK FOR THE VALID KEY THAT WAS RETURNED FROM THE EMAIL */
    function check_valid($key){

       $query = $this->db->get_where('users', array('pw_recovery' => $key));

        if($query->num_rows() > 0) return TRUE;
    }

    /* UPDATE THE USERS PASSWORD CONSIDERING THAT THE KEY FROM THE EMAIL IS VALID. */
    function new_password()
    {
        $query = $this->db->get_where('users', array('pw_recovery' => $this->input->post('key')));

        if($query->num_rows() > 0 ) :

            //HOUSTON, WE FOUND A MATCH! LET'S UPDATE THIS USERS NEW PASSWORD
            $this->db->set('password', $this->input->post('password') );
            $this->db->set('pw_recovery', 0);
            $this->db->where('pw_recovery', $this->input->post('key'));            
            $this->db->update('users');
        endif;
    }

    function delete_user()
    {
        $this->db->where('users_id', $this->uri->segment(4));
        $this->db->delete($this->table);
    }

    function delete_account()
    {
        $this->db->set('identity', 0 );
        $this->db->set('is_logged_in', 0 );
        $this->db->where('email', $this->session->userdata('email'));
        $this->db->update('users');
        $this->session->sess_destroy();

        $this->db->where('users_id', $this->input->post('delete_account'));
        $this->db->delete($this->table);
    }

    function profile_update($id_user)
    {
        if ( $this->input->post('password') == "") :

            $data = array(
               'first_name' => $this->input->post('first_name'),
               'last_name'  => $this->input->post('last_name'),
               'email'      => $this->input->post('email'),
               'about'      => $this->input->post('about')
            );
        else:
            $data = array(
               'first_name' => $this->input->post('first_name'),
               'last_name'  => $this->input->post('last_name'),
               'email'      => $this->input->post('email'),
               'password'   => sha1($this->input->post('password')),
               'about'      => $this->input->post('about')
            );
        endif;
        $this->db->where('users_id', $id_user);
        $this->db->update('users', $data);
    }
} //END USERS_MODEL