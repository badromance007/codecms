<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Posts_model extends CI_Model {	

	public $database 			= 'codecms';
	public $posts_table 		= 'posts';
	public $settings_table 		= 'settings';
	public $users_table 		= 'users';
	public $cats_table 			= 'categories';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();        
    }	

	/* ===============================================================	BACK END =============================================================== */

	//MULTIPLE DELETE
	function delete_post_selection($selectedIds)
	{
	    $this->db->where_in('post_id', $selectedIds)->delete('posts');
	}	

	function update_post_settings()
	{

		$data = array(
		    array(
		        'settings_id' 		=> 1,
		        'settings_name' 	=> 'POST_PAGE_CHOSEN',
		        'settings_value' 	=> $this->input->post('POST_PAGE_CHOSEN')
		    ),
		    array(
		        'settings_id' 		=> 2,
		        'settings_name'		=> 'POST_PER_PAGE',
		        'settings_value' 	=> $this->input->post('POST_PER_PAGE')
		    ),
		    array(
		        'settings_id' 		=> 3,
		        'settings_name' 	=> 'ARRANGE_POST_BY',
		        'settings_value' 	=> $this->input->post('ARRANGE_POST_BY')
		   	),
		   	array(
		        'settings_id' 		=> 4,
		        'settings_name' 	=> 'ORDER_POST_BY',
		        'settings_value' 	=> $this->input->post('ORDER_POST_BY')
		   	),
		   	array(
		        'settings_id' 		=> 5,
		        'settings_name' 	=> 'DEFAULT_EMAIL',
		        'settings_value' 	=> $this->input->post('DEFAULT_EMAIL')
		   	)		    
		);

		$this->db->update_batch('settings', $data, 'settings_id');

	}

	function settings() 
	{
		$query = $this->db->get('settings');

		if ( $query->num_rows() > 0 ) :
			return $query->result_array();
		endif;	
	}

	//CHECK FOR THE PAGE THAT WILL SHOW ALL THE POSTS. THIS IS SET IN THE DATABASE BY THE SETTINGS.
	function check_post_page()
	{
		$query = $this->db->get('settings');

		if ( $query->num_rows() > 0 ) :
			foreach ($query->result_array() as $value) :
				return $value;
			endforeach;
		endif;	

	}

	/* ===============================================================	FRONT END =============================================================== */
	
    function view_post($slug)
    {
        $query = $this->db->get_where('posts', array('slug' => $slug ), 1);
		
		if($query->num_rows() == 1):
 			return $query->row();
		endif;
    }

    function query_post( $id )
    {
    	$this->db->select('post_id, title, content');
    	$this->db->where('post_id', $id);
    	$query = $this->db->get('posts', 1);

    	if ( $query->num_rows() > 0 ) :
    		return $query->row();
    	endif;
    }

    function build_menu( $excl = array('null') )
    {
    	$sql = '
			SELECT p.title, p.status, p.slug
			FROM cc_posts AS p
			WHERE p.post_id IN ( '. implode(",", $excl ).' )
			AND p.post_type = "page"
    	';

    	$query = $this->db->query($sql);

    	if ( $query->num_rows() > 0 ) :
    		return $query->result_array();
    	endif;    	
    }

    function getCategory($id)
    {
    	$this->db->select('cat_id, name');
    	$this->db->where('cat_id', $id);
    	$query = $this->db->get($this->cats_table, 1);

    	if ( $query->num_rows() > 0 ) :
    		return $query->row();
    	endif;    	
    }
    //CREATE CATEGORY
/*    function insert_category()
    {
    	$data = array('name' => $this->input->post('name') );
		$this->db->insert($this->cats_table, $data);
    }*/

    function update_category($id)
    {
		$data = array(
			'name' 	=> $this->input->post('name')
		);

		$this->db->where('cat_id', $id);
		$this->db->update($this->cats_table, $data);
    }

	function cat_delete($id)
	{
		//DELETE A CATEGORY
		$this->db->where('cat_id', $id);
		$this->db->delete($this->cats_table);
	}    
}