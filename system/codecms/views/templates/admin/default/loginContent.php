<?php 
	$attr = array('class' => 'form-signin', 'id' => 'myform');
	echo form_open('admin/login', $attr); 
?>
	<h2 class="form-signin-heading">Log In Now</h2>

	<?php if ( validation_errors() ) : ?>
		<div class="alert-block alert-error fade in">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo validation_errors(); ?> 
		</div>
	<?php endif; ?>

	<?php if( $this->session->flashdata('error')) :  ?>
		<div class="alert-block alert-error fade in">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo $this->session->flashdata('error') ?> 
		</div>
	<?php elseif( $this->session->flashdata('success')) : ?>
		<div class="alert-block alert-success fade in">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo $this->session->flashdata('success') ?>
		</div>
	<?php endif; ?>		

	<input type="text" class="input-block-level" name="email" placeholder="Email address" value="<?php echo $this->input->post('email'); ?>">
	<input type="password" class="input-block-level" name="password" placeholder="Password" value="">
	<label for="forgot_password">
	 <a href="<?php echo base_url('password'); ?>">Forgot Password?</a>
	</label>
	<button class="btn btn-sml btn-primary" type="submit">Log in</button>
<?php echo form_close(); ?>