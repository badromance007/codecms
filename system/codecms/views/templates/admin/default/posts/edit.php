<div class="row clearfix:after pages_list">
	
	<div class="span3 bs-docs-sidebar">

		<!-- LOAD THE PAGE SIDEBAR -->
		<?php $this->load->view('templates/admin/default/shared/sidebar'); ?>
		
	</div>

	<div class="span9">
		<section id="posts">
			<div class="controlgroup">

				<?php 

				//POST EDIT FORM

				$attr = array('class' => 'clear', 'id' => 'post_edit');
				echo form_open('admin/posts/edit', $attr); ?>

				<div class="controls clearfix">
					<input type="hidden" name="slug" value="<?php echo $posts->slug; ?>">
					<input type="hidden" name="post_id" value="<?php echo $posts->post_id; ?>">
					<label for="post_title">Post Title <sup class="text-error">*</sup></label>
					<input type="text" class="input-block-level" name="title" value="<?php echo $posts->title; ?>">
				</div>

				<div class="controls clearfix">
					<textarea name="content" id="content" class="input-block-level ckeditor" cols="30" rows="10"><?php echo $posts->content; ?></textarea>
				</div>

				<div class="controls">
					<label>Publishing options:</label>
					<div class="controlgroup clearfix">
						<select name="status">
							<option value="unpublished" <?php if ( $posts->status == 'unpublished') : ?>selected="selected"<?php endif; ?>>Unpublished</option>
							<option value="published" <?php if ( $posts->status == 'published') : ?>selected="selected"<?php endif; ?>>Published</option>
						</select>
					</div>					
				</div>

				<div class="controls">
					<label>Categories</label>
					<div class="control-group clearfix">
						<select name="post_cat">
							<option value="0">Select Category</option>
							<?php foreach ($categories as $category ) : ?>
								<option <?php if ($posts->post_cat == $category['cat_id']) : ?>selected="selected"<?php endif; ?> value="<?php echo $category['cat_id'] ?>"> <?php echo $category['name'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>				


				<div class="controls">
					<a href="<?php echo base_url('admin/admin_posts/posts_list'); ?>" class="btn btn-info">Back</a>
					<input type="submit" id="edit" name="edit" class="btn btn-primary" value="Save" />
				</div>
				<?php echo form_close(); ?>
			</div>
		</section>
	</div>	
</div>