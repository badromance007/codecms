<?php $attr = array('id' => 'profile-update'); echo form_open("admin/profile/update"); ?>
	<section id="personal_info">
		<div class="controlgroup">
			<div class="page-header">Personal Information</div>
			<div class="controls">
				<label for="username" class="muted"> <i class="icon-fire">&nbsp;</i> Username: </label>
				<input type="text" name="username" id="username" value="<?php echo $logged_info['username']; ?>" readonly />
			</div>

			<div class="controls">
				<label for="first_name" class="muted"><i class="icon-user">&nbsp;</i> First Name:</label>
				<input type="text" name="first_name" id="first_name" value="<?php echo $logged_info['first_name']; ?>" />
			</div>

			<div class="controls">
				<label for="last_name" class="muted"><i class="icon-user">&nbsp;</i> Last Name:</label>
				<input type="text" name="last_name" id="last_name" value="<?php echo $logged_info['last_name']; ?>" />
			</div>

			<div class="controls">
				<label for="email" class="muted"><i class="icon-envelope">&nbsp;</i> Email:</label>
				<input type="text" name="email" id="email" value="<?php echo $logged_info['email']; ?>" />
			</div>

			<div class="controls">
				<label for="password" class="muted"><i class="icon-lock">&nbsp;</i> Password:</label>
				<input type="password" name="password" value="" />
				<span class="text-error">Leave it blank if you do not wish to change password</span>
			</div>
			<div class="controls">
				<label for="about" class="muted"><i class="icon-lock">&nbsp;</i> About You:</label>
			</div>
			<div class="controls">
				<textarea name="about" id="about" cols="30" rows="10" class="ckeditor">
					<?php echo $logged_info['about']; ?>
				</textarea>
			</div>
			<div class="controls">
				<button class="btn btn-primary" name="id_user" value="<?php echo $this->uri->segment(4); ?>">Save Profile</button>
			</div>
		</div>
	</section>
</form>
<?php echo form_close(); ?>