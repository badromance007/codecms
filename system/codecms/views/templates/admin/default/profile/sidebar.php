<?php $users_id = $logged_info['users_id']; ?>
<ul class="nav nav-list bs-docs-sidenav affix-top">
	<li <?php if ( $this->uri->segment(2) == 'profile' && !$this->uri->segment(3) == 'update') : echo "class='active'"; endif; ?>><a href="<?php echo base_url("admin/profile"); ?>">Personal Information</a></li>
	<li <?php if ( $this->uri->segment(2) == 'profile' && $this->uri->segment(3) == 'update' ): echo "class='active'"; endif; ?>><a href="<?php echo base_url("admin/profile/update/$users_id") ?>">Update your profile</a></li>	
	<li><a href="#">Social</a></li>
</ul>