
<ul class="nav nav-list bs-docs-sidenav affix-top">
	<li <?php if ( $this->uri->segment(2) == 'users' && !$this->uri->segment(3) == 'create') : echo "class='active'"; endif; ?>><a href="<?php echo base_url('admin/users'); ?>">Manage Users</a></li>
	<li <?php if ( $this->uri->segment(2) == 'users' && $this->uri->segment(3) == 'create') : echo "class='active'"; endif; ?>><a href="<?php echo base_url("admin/users/create"); ?>">New User</a></li>
</ul>