
<?php 

	$attr = array('class' => 'form-signin', 'id' => 'myform');
	  echo form_open('password', $attr);
	?>
	<h2 class="form-signin-heading">Forgot Password</h2>

	<?php if ( validation_errors() ) :  ?>
		<div class="alert-block alert-error fade in">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo validation_errors(); ?> 
		</div>
	<?php endif; ?>

	<?php if( $this->session->flashdata('error')) :  ?>
		<div class="alert-block alert-error fade in">
			<a class="close" data-dismiss="alert">&times;</a>
			<?php echo $this->session->flashdata('error') ?>
		</div>
	<?php else: ?>
		<div class="text-success"> <?php echo $this->session->flashdata('success') ?> </div>	
	<?php endif; ?>	

	<input type="email" class="input-block-level" name="email" placeholder="Enter email address" value="<?php echo $this->input->post('email'); ?>">

	<a href="<?php echo base_url('admin'); ?>" class="btn btn-sml btn-info">Go back</a>
	<button class="btn btn-sml btn-primary" type="submit" name="password">Retrieve Password</button>
<?php echo form_close(); ?>