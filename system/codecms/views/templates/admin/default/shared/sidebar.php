<?php if ( $logged_info['role'] == 'admin') : ?>
  <div class="accordion" id="accordion2">
    <div class="accordion-group">
      <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#tab_pages">Manage Pages</a>
      </div>
      <div id="tab_pages" class="accordion-body <?php if ( ( $this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'pages' ) ) : echo "in"; else : echo "collapse"; endif; ?>">
        <div class="accordion-inner">
      		<ul class="nav nav-list">
            <li <?php if ( ( $this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'pages' && !$this->uri->segment(3) == 'create' ) ) : echo "class='active'"; endif; ?>> <a href="<?php echo base_url('admin/pages'); ?>">List Pages </a></li>
      			<li <?php if ( ( $this->uri->segment(2) == 'pages' && $this->uri->segment(3) == 'create' ) ) : echo "class='active'"; endif; ?>>
              <a href="<?php echo base_url('admin/pages/create'); ?>">New page</a>
            </li>
      		</ul>
        </div>
      </div>
    </div>
    <div class="accordion-group">
      <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#tab_posts">Manage Posts</a>
      </div>
      <div id="tab_posts" class="accordion-body <?php if ( ( $this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'posts' ) || ( $this->uri->segment(2) == 'post' && $this->uri->segment(3) == 'edit' ) || ( $this->uri->segment(2) == 'post' && $this->uri->segment(3) == 'create' ) ) : echo "in"; else : echo "collapse"; endif; ?>">
        <div class="accordion-inner">
  		<ul class="nav nav-list">
          <li <?php if ( ( $this->uri->segment(2) == 'posts' && !$this->uri->segment(3) == 'create' ) ): echo "class='active'"; endif; ?>><a href="<?php echo base_url('admin/posts'); ?>">List Posts</a></li>
          <li <?php if ( ( $this->uri->segment(2) == 'posts' && $this->uri->segment(3) == 'create' ) ): echo "class='active'"; endif; ?>><a href="<?php echo base_url('admin/posts/create'); ?>">New Post</a></li>
  		</ul>
        </div>
      </div>
    </div>
    <div class="accordion-group">
      <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#tab_categories">Manage Categories</a>
      </div>
      <div id="tab_categories" class="accordion-body <?php if ( ( $this->uri->segment(1) == 'admin' && $this->uri->segment(2) == 'categories' ) ) : echo "in"; else : echo "collapse"; endif; ?>">
        <div class="accordion-inner">
      <ul class="nav nav-list">
          <li <?php if ( ( $this->uri->segment(2) == 'categories' && !$this->uri->segment(3) == 'create' ) ) : echo "class='active'"; endif; ?>><a href="<?php echo base_url('admin/categories'); ?>">List Categories</a></li>
          <li <?php if ( ( $this->uri->segment(2) == 'categories' && $this->uri->segment(3) == 'create' ) ): echo "class='active'"; endif; ?>><a href="<?php echo base_url('admin/categories/create'); ?>">Create Category</a></li>
      </ul>
        </div>
      </div>
    </div>    
  </div>
<?php endif; ?>