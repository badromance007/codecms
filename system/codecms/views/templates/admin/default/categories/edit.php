<div class="row clearfix:after pages_list">
	
	<div class="span3 bs-docs-sidebar"><?php $this->load->view('templates/admin/default/shared/sidebar'); ?></div>

	<div class="span9">
		<section id="pages">
			<div class="controlgroup">

				<?php

				//CATEGORY EDIT FORM

				$attr = array('id' => 'cat_edit');
				echo form_open('admin/categories/edit', $attr); ?>

				<div class="controls clearfix">
					<label for="cat_title">Category Title <sup class="text-error">*</sup></label>
					<input type="text" class="input-block-level" id="cat_title" name="name" value="<?php echo $category->name ?>">
					<input type="hidden" name="cat_id" value="<?php echo $this->uri->segment(4) ?>">
				</div>

				<div class="controls">
					<a href="<?php echo base_url('admin/categories'); ?>" class="btn btn-info">Go Back</a>
					<input type="submit" name="cat_edit" class="btn btn-primary" value="Edit category" />
				</div>
				<?php echo form_close(); ?>
			</div>
		</section>
	</div>
</div>