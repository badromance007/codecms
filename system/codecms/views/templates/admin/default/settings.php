<ul id="cc_tabs" class="nav nav-tabs">
  <li><a href="#users" data-toggle="tab">Users</a></li>
  <li><a href="#pages" data-toggle="tab">Pages</a></li>
  <li class="active"><a href="#posts" data-toggle="tab">Posts</a></li>
  <li><a href="#others" data-toggle="tab">Others</a></li>
</ul>
 
<div class="tab-content">
	<div class="tab-pane" id="users">Users settings</div>
	<div class="tab-pane" id="pages">Pages settings</div>
	<div class="tab-pane active" id="posts">
	
	<?php echo form_open('admin/settings'); ?>
	
		<?php if ( validation_errors() ) :  ?>
		  <div class="alert-block alert-error fade in">
		  	<a class="close" data-dismiss="alert">&times;</a>
		    <?php echo validation_errors(); ?> 
		  </div>
		<?php endif;  ?>		
										
		<div class="controlgroup">
			
			<?php foreach ($post_settings as $setting) : ?>

				<?php if ($setting['settings_name'] == 'POST_PAGE_CHOSEN') : ?>
					<div id="choose_page" class="controls">
						<label for="POST_PAGE_CHOSEN" class="input-block-level">Page to display your posts. </label>	
						<select name="POST_PAGE_CHOSEN" id="POST_PAGE_CHOSEN" class="clear">
							<option value="0">Choose Page</option>
							<?php if ( isset($pages)) : ?>
								<?php foreach ( $pages as $page ) : ?>
									<?php if ( $page['status'] == 'published' && $page['slug'] == $POST_PAGE_CHOSEN ) : ?>
			  							<option selected="selected" value="<?php echo $page['slug']; ?>"><?php echo $page['title']; ?></option>
			  						<?php else: ?>
				  						<option value="<?php echo $page['slug']; ?>"><?php echo $page['title']; ?></option>
									<?php endif; ?> 
								<?php endforeach; ?>
							<?php endif; ?>	
						</select>
					</div>
				<?php endif; ?>			

				<?php if ($setting['settings_name'] == 'POST_PER_PAGE') : ?>
					<div class="controls">
						<label for="POST_PER_PAGE" class="input-block-level">Number of post to show in the page.</label>
						<input type="text" name="POST_PER_PAGE" placeholder="Posts per page" value="<?php echo set_value('10', $setting['settings_value']); ?>">
					</div>
				<?php endif; ?>

				<?php if ($setting['settings_name'] == 'ARRANGE_POST_BY') : ?>
					<div class="controls">
						<label for="ARRANGE_POST_BY" class="input-block-level">Default arrange post by.</label>
						<select name="ARRANGE_POST_BY" id="ARRANGE_POST_BY">
							<option value="post_id" <?php if ( $setting['settings_value'] == 'post_id' ) : ?>selected="selected"<?php endif; ?>>By ID</option>
							<option value="date_add" <?php if ( $setting['settings_value'] == 'date_add' ) : ?>selected="selected"<?php endif; ?>>By Date</option>
						</select>
					</div>
				<?php endif; ?>

				<?php if ($setting['settings_name'] == 'ORDER_POST_BY') : ?>

					<div class="controls">
						<label for="ORDER_POST_BY" class="input-block-level">Default order post by.</label>
						<select name="ORDER_POST_BY" id="ORDER_POST_BY">
							<option value="asc" <?php if ( $setting['settings_value'] == 'asc' ) : ?>selected="selected"<?php endif; ?>>Ascending</option>
							<option value="desc" <?php if ( $setting['settings_value'] == 'desc' ) : ?>selected="selected"<?php endif; ?>>Descending</option>
						</select>
					</div>
				<?php endif; ?>

				<?php if ($setting['settings_name'] == 'DEFAULT_EMAIL') : ?>

					<div class="controls">
						<label for="DEFAULT_EMAIL" class="input-block-level">Default email</label>
						<input type="text" id="DEFAULT_EMAIL" name="DEFAULT_EMAIL" value="<?php echo set_value('', $setting['settings_value']); ?>" />
					</div>
				<?php endif; ?>
			<?php endforeach; ?>

			<div class="controls">
				<button name="save_post_settings" class="btn btn-primary btn-small"><i class="icon-ok icon-white"> &nbsp; </i> Save Post Settings</button>  			
			</div>
		</div>
	
	<?php echo form_close(); ?>			

	</div>
	<div class="tab-pane" id="others">Other Settings</div>
</div>