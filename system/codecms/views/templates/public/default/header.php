<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->template->meta; ?>
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title><?php echo $this->template->title->default("Page Not Found"); ?></title>

    <?php echo $this->template->stylesheet; ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container site-wrapper">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>">CodeCMS</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <?php if ( isset($pages) ): ?>
              <?php foreach ($pages as $page) : ?>
                <?php if( $page['status'] == 'published') : $slug = $page['slug']; ?>
                  <li>
                    <a href="<?php echo base_url("$slug")  ?>">
                      <?php echo $page['title']; ?>
                    </a>
                  </li>
                <?php endif; ?>
              <?php endforeach; ?> 
            <?php endif; ?>
            <li><a href="<?php echo base_url('contact') ?>">Contact Us</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">