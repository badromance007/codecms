<section id="main_content" class="pages">
	<?php if ( isset($page) ) : ?>
		<h2><?php echo $page->title; ?></h2>

		<?php echo $page->content; ?>

	<?php else: ?>
		<p class="text-error">Sorry, there are no page to show or page is not existing. </p>
	<?php endif; ?>
</section>