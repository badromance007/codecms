<!-- THIS IS THE ACTUAL PAGE CONTENT-->
<div class="row-fluid posts_tpl container">
	<section id="main_content" class="contact well">
		<fieldset>
			<legend>Contact Us</legend> <br />
			<?php echo form_open_multipart('contact/submit', 'role="form" id="form_submit" class="form-horizontal'); ?>

			<?php if ( validation_errors() ) :  ?>
			  <div class="alert alert-danger fade in">
			  	<a class="close" data-dismiss="alert">&times;</a>
			    <?php echo validation_errors(); ?> 
			  </div>
			<?php endif;  ?>

			<?php if ( $this->session->flashdata('success') ) : ?>
				<div class="alert alert-success fade in">
				  <a class="close" data-dismiss="alert">&times;</a>
				  <?php echo $this->session->flashdata('success'); ?> 
				</div>
			<?php elseif ( $this->session->flashdata('error') ): ?>
				<div class="alert alert-danger fade in"> 
				  <a class="close" data-dismiss="alert">&times;</a>
				  <?php echo $this->session->flashdata('error'); ?> 
				</div>      
			<?php endif; ?>

			<div class="form-group">
				<div class="col-sm-4">
					<label for="first_name" class="control-label">First Name: <span class="text text-danger">*</span></label> 
					<input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" value="<?php echo $this->input->post('first_name'); ?>" />			
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-4">
					<label class="control-label" for="last_name">Last Name:</label>
				 	<input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" value="<?php echo $this->input->post('last_name'); ?>" />
				 </div>
			</div>

			<div class="form-group">
			    <div class="col-sm-4">
			    	<label class="control-label" for="email">Email: <span class="text text-danger">*</span></label>
			     	<input type="text" name="email" id="email" class="form-control" placeholder="Email" value="<?php echo $this->input->post('email'); ?>" />	      
			     </div>
			</div>

			<div class="form-group">
			    <div class="col-sm-4">
			    	<label class="control-label" for="phone">Mobile Number: <span class="text text-danger">*</span></label>
			     	<input type="text" name="phone" id="phone" class="form-control" placeholder="+63xxx" value="<?php echo $this->input->post('phone'); ?>" />	      
			     </div>
			</div>			

			<div class="form-group">
			    <div class="col-sm-4">
			    	<label class="control-label" for="website">Website:</label>
			     	<input type="text" name="website" id="website" class="form-control" placeholder="Website" value="<?php echo $this->input->post('website'); ?>" />
			     </div>
			</div>

			<div class="form-group">
				<div class="col-sm-4">
					<label for="reason" class="control-label">How may I help you?: <span class="text text-danger">*</span></label>
					<input type="text" name="reason" id="reason" class="form-control" placeholder="How may I help you?" value="<?php echo $this->input->post('reason'); ?>" />			
				</div>
			</div>	

			<div class="form-group">
				<div class="col-sm-4">
					<label class="control-label" for="message">Message:</label>
				 	<textarea name="message" id="message" class="form-control" cols="30" rows="10" placeholder="Your Message" value="<?php echo $this->input->post('message'); ?>"></textarea>
				 </div>
			</div>

			 <button type="submit" name="send_message" class="btn btn-primary">Send Message</button>

			<?php echo form_close(); ?>
		</fieldset>	
	</section>
</div>