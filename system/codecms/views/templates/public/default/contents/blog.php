<div class="row">
    <?php if ( isset($posts) && is_array($posts) && $posts ) : ?>
            <ul class="pull-left col-md-8 well blog list-unstyled">
                <?php foreach ($posts as $post) : ?>
                    <li>
                        <div class="clearfix spacer dashed"></div>
                            <div class="blog-header">
                              <h1 class="blog-title"><?php echo $post['title']; ?></h1>
                            </div>        

                            <div class="col-sm-8 blog-main">
                                <div class="blog-post">
                                    <?php echo $post['content']; ?>
                                </div><!-- /.blog-post -->
                            </div><!-- /.blog-main -->
                    </li>
                <?php endforeach; ?>
            </ul>

            <div class="col-sm-3 col-sm-offset-1 col-md-3 blog-sidebar pull-right">
                <div class="sidebar-module">
                  <h4>Archives</h4>
                  <ol class="list-unstyled">
                    <li><a href="#">January 2014</a></li>
                    <li><a href="#">December 2013</a></li>
                    <li><a href="#">November 2013</a></li>
                    <li><a href="#">October 2013</a></li>
                    <li><a href="#">September 2013</a></li>
                    <li><a href="#">August 2013</a></li>
                    <li><a href="#">July 2013</a></li>
                    <li><a href="#">June 2013</a></li>
                    <li><a href="#">May 2013</a></li>
                    <li><a href="#">April 2013</a></li>
                    <li><a href="#">March 2013</a></li>
                    <li><a href="#">February 2013</a></li>
                  </ol>
                </div>
            </div><!-- /.blog-sidebar -->    
    <?php else: ?>

        <?php if ( isset($page) && $page) : ?>
        
        <div class="blog-header">
          <h1 class="blog-title"><?php echo $page->title; ?></h1>
        </div>

        <div class="row">
          <div class="col-sm-8 blog-main">
            <div class="blog-post well">
                <?php echo $page->content; ?>
            </div><!-- /.blog-post -->
          </div><!-- /.blog-main -->
          <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
            <div class="sidebar-module">
              <h4>Archives</h4>
              <ol class="list-unstyled">
                <li><a href="#">January 2014</a></li>
                <li><a href="#">December 2013</a></li>
                <li><a href="#">November 2013</a></li>
                <li><a href="#">October 2013</a></li>
                <li><a href="#">September 2013</a></li>
                <li><a href="#">August 2013</a></li>
                <li><a href="#">July 2013</a></li>
                <li><a href="#">June 2013</a></li>
                <li><a href="#">May 2013</a></li>
                <li><a href="#">April 2013</a></li>
                <li><a href="#">March 2013</a></li>
                <li><a href="#">February 2013</a></li>
              </ol>
            </div>
          </div><!-- /.blog-sidebar -->
        </div><!-- /.row -->
        <?php else: ?>
            <p class="alert alert-danger">Sorry, no page available. </p>
        <?php endif; ?>
    <?php endif; ?>
</div><!-- /.row -->    