<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Profile extends Admin_Controller 
{

    public $table   = 'users';
    public $column  = 'users_id'; 

    public function index()
    {
        $this->template->set_template('templates/admin/default/profile/index');
        $this->template->title  = 'Profile page';

        $data['logged_info']    = $this->users_model->logged_in();
        $this->template->content->view('templates/admin/default/profile/content', $data);
        $this->template->publish();
    }

    public function update()
    {   
        $this->template->set_template('templates/admin/default/profile/index');
        $data['logged_info']    = $this->users_model->logged_in();

        if ( $this->input->post('id_user') ) :
            
            //MAKE SURE YOU ARE UPDATING ONLY YOUR ACCOUNT
            //COMPARE THE SESSION USER ID AND THE POSTED USER ID
            if ( $this->input->post('id_user') == $this->id_user ) :

                //CHECK IF YOU ARE UPDATING PASSWORD
                if ($this->input->post('password')) :

                    //VALIDATE THE PASSWORD FIELD
                    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|sha1');

                    if ( $this->form_validation->run() === TRUE ) :
    
                        $params = array(
                           'first_name' => $this->input->post('first_name'),
                           'last_name'  => $this->input->post('last_name'),
                           'email'      => $this->input->post('email'),
                           'about'      => $this->input->post('about'),
                           'password'   => $this->input->post('password')
                        );

                        //UPDATE YOUR PROFILE WITH PASSWORD CHANGE
                        $this->db_calls_model->update($this->table, $this->column, $this->input->post('id_user'), $params);
                        
                        $data["success"] = $this->session->set_flashdata("success", "You have successfully updated your profile.");
                        redirect("admin/profile/update/$this->id_user", $data);                        
                    endif;
                else:

                    $params = array(
                       'first_name' => $this->input->post('first_name'),
                       'last_name'  => $this->input->post('last_name'),
                       'email'      => $this->input->post('email'),
                       'about'      => $this->input->post('about')
                    );

                    //UPDATE YOUR PROFILE
                    $this->db_calls_model->update($this->table, $this->column, $this->input->post('id_user'), $params);
                    
                    $data["success"] = $this->session->set_flashdata("success", "You have successfully updated your profile.");
                    redirect("admin/profile/update/$this->id_user", $data);

                endif;
            else:
                $data["error"] = $this->session->set_flashdata("error", "Ooops, you are trying to update someone else account. It is prohibitted!.");
                redirect("admin/profile/update/$id_user", $data);
            endif;  
        endif;

        $this->template->title  = 'My Profile Update page';
        $this->template->content->view('templates/admin/default/profile/update', $data);
        $this->template->publish();    
    }
}