<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeCMS an alternative responsive open source cms made from Philippines.
 *
 * @package     CodeCMS
 * @author      @jsd
 * @copyright   Copyright (c) 2013
 * @license     http://creativecommons.org/licenses/by-sa/3.0/deed.en_US
 * @link        https://bitbucket.org/jsdecena/codecms
 * @since       Version 0.1
 * 
 */

class Contact extends Front_Controller {

	function __construct(){

		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('settings_model');
	}

	public function index($post_type = 'page'){

        $this->template->title      = 'Contact Page';
        
        //ALL THE PAGES FOR THE MENU PAGE LISTING
        $params 					= array('post_type' => 'page', );
        $data['pages']              = $this->db_calls_model->query_post_array('posts', $params);

        $this->template->content->view('templates/public/default/contents/contact', $data);
        
        // publish the template
        $this->template->publish(); 
	}

	public function submit()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|xss_clean|alpha');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email');
		$this->form_validation->set_rules('phone', 'Mobile Number', 'trim|required|numeric|xss_clean');
		$this->form_validation->set_rules('reason', '"How may I help you"', 'trim|required|xss_clean');

		
		$config = array(
			        'mailtype'  => 'html',
			        'starttls'  => true,
        			'newline'   => "\r\n",
        			'charset'	=> 'utf-8',
				);

		$this->load->library('email', $config);
		$this->email->set_crlf("\r\n");

		if ( $this->form_validation->run() ) :

			$this->email->from($this->input->post('email'), $this->input->post('first_name'));
			$this->email->to($this->settings_model->get_setting('DEFAULT_EMAIL')); 			//CHANGE TO YOUR EMAIL ADDRESS
			$this->email->subject("Website Inquiry");

			$data['first_name'] 	= $this->input->post('first_name');
			$data['last_name'] 		= $this->input->post('last_name');
			$data['email'] 			= $this->input->post('email');
			$data['phone'] 			= $this->input->post('phone');
			$data['website'] 		= $this->input->post('website');
			$data['message'] 		= $this->input->post('message');
		    $data['reason'] 		= $this->input->post('reason');

			$message = $this->load->view('templates/email/contact', $data, TRUE );

			$this->email->message($message);

			if ($this->email->send()) :

				$data['success'] = $this->session->set_flashdata('success', 'You have successfully sent an email.');
				redirect('contact', $data);
			else:

				$data['error'] = $this->session->set_flashdata('error', 'Sorry, we there is a problem in sending your message. Please try again.');
				redirect('contact', $data);
			endif;

		else:
			//IF THE USER FAILED THE FORM VALIDATION, GO BACK TO CONTACT PAGE WITH THE ERRORS
			$this->index();
		endif;
	}
}